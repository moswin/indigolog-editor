package au.edu.rmit.indigologeditor.editors;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;


// Validates the number of arguments for each predicate, creates markers for errors.
public class ValidateNumArgs {
	
	//Have document to validate against?
	//Update document?
	//private Document doc;
	public static void validateAllPredicateArgs(IResource res){
		checkNumArgs("poss", 2, res);
		checkNumArgs("prim_action", 1, res);
		checkNumArgs("prim_fluent", 1, res);
		checkNumArgs("causes_val", 4, res);
		//variable number of arguments?
		//checkNumArgs("causes", 4, res);
		checkNumArgs("initially", 2, res);
		checkNumArgs("rejects", 5, res);
		checkNumArgs("settles", 5, res);

		//Need to find out number of arguments
		//checkNumArgs("proc", 2, res);
		checkNumArgs("senses", 1, res);
		checkNumArgs("exog_action", 1, res);
		checkNumArgs("cache", 1, res);
		checkNumArgs("fun_fluent", 1, res);
	}

	// Call to set error markers for incorrect predicate calls
	public static void checkNumArgs(String pattern, int corrNumArgs, IResource resource){
		
		Pattern funcName = Pattern.compile(pattern);
		int numArgs;
		int lineNum = 1;
		String filePath = IndiGologTextValidator.getFilePath(resource);
		System.out.println("FilePath: " + filePath);
		
		//File reading
		try{
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			String line;
			while((line = br.readLine())!=null){
				Matcher m = funcName.matcher(line);
				StringBuilder sb = new StringBuilder();
				if(m.find()){
					System.out.println();
					System.out.println("Found a match on line: " + lineNum);
					int offset = m.end();
					int openBracket = 0;
					if(line.charAt(offset) == '('){
						openBracket++;
						offset++;
						while(openBracket!=0 && offset < line.length()){
							if(line.charAt(offset)=='('){
								openBracket++;
							}
							else if(line.charAt(offset)==')'){
								openBracket--;
								if(openBracket == 0){
									break;
								}
							}						
							sb.append(line.charAt(offset));
							offset++;
						}
						
						String[] tempRes = sb.toString().split(",");
						String[] res = new String[tempRes.length];
						
						//try and find sub strings with commas
						
						for(int i = 0; i < tempRes.length; i++){
							String token = tempRes[i];
							Boolean tokenAdded = false;
							
							if(tempRes[i].contains("(")){
								if(!tempRes[i].contains(")")){
									for (int j = i+1; j < tempRes.length; j++){
										token = token + "," + tempRes[j];
										if(token.contains(")")){
											System.out.println("Token Added: " + token);
											res[i] = token;
											tokenAdded = true;
											i = j;
											break;
										}
									}
								}
							}
							
							if(tokenAdded == false){
								res[i] = token;
								System.out.println("Token: " + token);
							}
						}

//						numArgs = res.length;
						numArgs = 0;
						for(int i = 0; i < res.length; i++){
							if(res[i]!=null){
								numArgs++;
							}
						}
						System.out.println("Number of Arguments: " + numArgs);
						
						if(numArgs != corrNumArgs){
							//add error marker on lineNum
							System.out.println("Error: Incorrect number of parameters on line " + lineNum);
							IndiGologMarkers.createMarker(resource, ("Error: Incorrect number of parameters on line " + lineNum), lineNum);
						}
					}
					else{
						//ignore not a correct function
					}
					
				}
				
				lineNum++;
			}
			br.close();
			
		}
		catch(IOException e){
			//TODO handle exception
			System.out.println("Couldn't open file");
		} 
		catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//validate poss and actions here will find a better place for it
		//ValidatePossAction.matchActionAndPoss(filePath.toString(), resource);
	}
}
