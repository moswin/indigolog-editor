package au.edu.rmit.indigologeditor.editors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.wst.validation.AbstractValidator;
import org.eclipse.wst.validation.ValidationResult;
import org.eclipse.wst.validation.ValidationState;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;


public class IndiGologTextValidator extends AbstractValidator {
	
	// Only as required, not utilized
	public ValidationResult validate(IResource resource, int kind, ValidationState state, IProgressMonitor monitor) {
			ValidationResult result = new ValidationResult();

			System.out.println("working?");
			// Delete the bracket markers already in place (Errors may have been fixed since last validation)
			//IndiGologMarkers.deleteBracketMarkers(resource);
			IndiGologMarkers.deleteAllMarkers(resource);
			
			//IndiGologMarkers.deleteBracketMarkers(resource);
			IndiGologMarkers.deleteAllMarkers(resource);
			
			IDocumentProvider docProvider = new TextFileDocumentProvider();
			try {
				/* Get the document */
				docProvider.connect(resource);
				IDocument doc = docProvider.getDocument(resource);

				/* Check for unmatched, mark if found */
				Position unmatched = BracketMatcher.getUnmatchedBracket(doc);
				if(unmatched != null) {
					IMarker marker = IndiGologMarkers.createMarker(resource, "Error: Bracket mismatch detected at char '" + doc.getChar(unmatched.offset) + "'", doc.getLineOfOffset(unmatched.offset)+1);
				}
			} catch (CoreException | BadLocationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//Try to validate number of arguments
			ValidateNumArgs.validateAllPredicateArgs(resource);
			
			//Validate that actions have corresponding poss
			//ValidateActions validateActions = new ValidateActions(resource);
			
			//validate fun_fluents
			ValidateFluents validateFluents = new ValidateFluents(resource);
			return result;
	}
	
	public static String getFilePath(IResource res){
		IPath fileIPath = res.getLocation();
		String filePath = fileIPath.toString();
		return filePath;
	}
}
