package au.edu.rmit.indigologeditor.editors;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

// Stores colors used by syntax highlighting
public interface ColorConstants {
	RGB IG_COMMENT = new RGB(0, 128, 0);
	RGB PROC_INSTR = new RGB(0, 0, 128);
	RGB KEYWORD = new RGB(200, 0, 0);
	RGB DEFAULT = new RGB(0, 0, 0);
	RGB IG_STRING = new RGB(0, 0, 200);
	RGB OPERATOR = new RGB(0,100,200);
}

