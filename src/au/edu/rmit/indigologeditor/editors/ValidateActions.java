package au.edu.rmit.indigologeditor.editors;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public class ValidateActions {
	
	private Map actions;
	private Map poss;
	private Map senses;
	private Map causes;
	private Map proc;
	private Map settles;
	private Map rejects;
	
	private IResource resource;
	
	public ValidateActions(IResource res){
		this.resource = res;
		
		//capture all the actions in predicates for comparison
		this.actions = getActionsFromPredicate(oneArgPattern("prim_action"));
		this.poss = getActionsFromPredicate(multiArgPattern("poss"));
		this.senses = getActionsFromPredicate(oneArgPattern("senses"));
		this.causes = getActionsFromPredicate(multiArgPattern("causes"));
		this.proc = getActionsFromPredicate(multiArgPattern("proc"));
		this.settles = getActionsFromPredicate(multiArgPattern("settles"));
		this.rejects = getActionsFromPredicate(multiArgPattern("rejects"));
		
		//make certain primative actions that have parameters start with uppercase letter
		//before comparing actions defined
		checkUppercaseParameters();
		
		//compare actions & poss
		this.compare(actions, poss, "Primative action ", " needs a corresponding poss");
		//compare poss & actions
		this.compare(poss, actions, "No primative action ", " has been defined");
		//compare senses & actions
		this.compare(senses, actions, "No primative action ", " has been defined");
		//compare causes & actions
//		this.compare(causes, actions, "No primative action ", " has been defined");
		//compare proc & actions
//		this.compare(proc, actions, "No primative action ", " has been defined");
		//compare settles & actions
		this.compare(settles, actions, "No primative action ", " has been defined");
		//compare rejects & actions
		this.compare(rejects, actions, "No primative action ", " has been defined");
		
	}

	public Map getActionsFromPredicate(Pattern pattern){
		String fileName = IndiGologTextValidator.getFilePath(this.resource);
		String line;
		int lineIndex = 1;
		Map actions = new HashMap();
		
		// capture all primitive action names and predicate action names
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				Matcher m = pattern.matcher(line);
				if (m.find()) {
					System.out.println("Action: " + m.group() + " on line "
							+ lineIndex);
					actions.put(m.group(), lineIndex);
				}
				lineIndex++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return actions;
	}
	
	public Boolean checkUppercaseParameters(){
		Boolean result = true;
		Pattern pattern = Pattern.compile("(?<=\\(|,)[^A-Z]{1}.*?(?=,|\\))");
		
		for (Object action : this.actions.keySet()){
			//remove whitespaces from string to get around java not allowing quantifiers in look ahead regex.
			String actionStripped = action.toString().replaceAll("\\s", "");
			System.out.println("Action stripped: " + actionStripped);
			Matcher m = pattern.matcher(actionStripped);
			if(m.find()){
				System.out.println("Match: " + m.toString());
				result = false;
				try {
					IndiGologMarkers.createMarker(this.resource,
							"parameters of primative actions must begin with an uppercase letter" + " on line " + actions.get(action),
							(int) actions.get(action));
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return result;
		
	}
	
	public void compare(Map actions, Map predicates, String errorPart1, String errorPart2){
		for (Object action : actions.keySet()) {
			if (!predicates.containsKey(action)) {
				try {
					IndiGologMarkers.createMarker(this.resource,
							errorPart1 + action + errorPart2 + " on line " + actions.get(action),
							(int) actions.get(action));
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public Pattern multiArgPattern(String predicate){
		Pattern actionName = Pattern.compile("(?<=" + predicate + "\\().*?(?=,\\s*([a-z]|\\d|\\[)+)");
		return actionName;
	}
	
	public Pattern oneArgPattern(String predicate){
		Pattern actionName = Pattern
				.compile("(?<=" + predicate + "\\().*?(?=\\)\\.|$)");
		return actionName;
	}
}
