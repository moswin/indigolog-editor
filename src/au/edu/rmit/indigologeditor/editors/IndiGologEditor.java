package au.edu.rmit.indigologeditor.editors;

import java.util.HashMap;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.source.DefaultCharacterPairMatcher;
import org.eclipse.jface.text.source.ICharacterPairMatcher;
import org.eclipse.core.internal.resources.Marker;
import org.eclipse.core.internal.runtime.Log;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.MarkerUtilities;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;

public class IndiGologEditor extends TextEditor {

	private ColorManager colorManager;
	
	public final static String MATCHING_BRACKETS = "matchingBrackets";
	public final static String MATCHING_BRACKETS_COLOR = "bracketsColor";

	public IndiGologEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new Configuration(colorManager));
		setDocumentProvider(new DocumentProvider());
		
		//attach annotation model to document
		IAnnotationModel model = getDocumentProvider().getAnnotationModel(this);
		IDocument document = getDocumentProvider().getDocument(this);
		
		//Create Vertical Ruler
		this.createVerticalRuler();
		
		if(document != null){
			this.getSourceViewer().setDocument(document, model);
		}
		
	}
	
	public void init(IEditorSite site, IEditorInput input)
            throws PartInitException {
         super.init(site, input);
         IResource resource = getResource();
         
         //delete markers on startup
         IndiGologMarkers.deleteAllMarkers(getResource());

         //test add marker probably call validate resource method here

	}
	
	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}
	
	protected IResource getResource() {
		IEditorInput input= this.getEditorInput();
		return (IResource) ((IAdaptable) input).getAdapter(IResource.class);
	}

	protected void configureSourceViewerDecorationSupport(SourceViewerDecorationSupport docSupport) {
		super.configureSourceViewerDecorationSupport(docSupport);
		
		char[] bracketChars = {'{', '}', '[', ']', '(', ')'};
		
		ICharacterPairMatcher pairMatcher = new DefaultCharacterPairMatcher(bracketChars, IDocumentExtension3.DEFAULT_PARTITIONING);
		docSupport.setCharacterPairMatcher(pairMatcher);
		docSupport.setMatchingCharacterPainterPreferenceKeys(MATCHING_BRACKETS, MATCHING_BRACKETS_COLOR);
		
		IPreferenceStore prefStore = getPreferenceStore();
		prefStore.setDefault(MATCHING_BRACKETS, true);
		prefStore.setDefault(MATCHING_BRACKETS_COLOR, "128, 128, 0");
	}
}
