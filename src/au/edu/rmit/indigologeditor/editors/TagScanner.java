package au.edu.rmit.indigologeditor.editors;

import org.eclipse.jface.text.*;
import org.eclipse.jface.text.rules.*;


// A generic tag scanner -- TODO: Can this be removed as well as taken from Configuration?
public class TagScanner extends RuleBasedScanner {

	public TagScanner(ColorManager manager) {
		IRule[] rules = new IRule[1];


		// Add generic whitespace rule.
		rules[0] = new WhitespaceRule(new WhitespaceDetector());

		setRules(rules);
	}
}