package au.edu.rmit.indigologeditor.editors;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.*;


// A scanner to detect strings within quotation marks and comments.
public class PartitionScanner extends RuleBasedPartitionScanner {
	public final static String IG_COMMENT = "__IG_comment";
	public final static String IG_STRING = "__IG_string";

	public PartitionScanner() {

		IToken igComment = new Token(IG_COMMENT);
		IToken igString = new Token(IG_STRING);

		IPredicateRule[] rules = new IPredicateRule[3];

		// Add rule for double quotes
		rules[0] = new SingleLineRule("\"", "\"", igString, '\\');
		// ...for single quotes
		rules[1] = new SingleLineRule("'", "'", igString, '\\');
		// ..for comments
		rules[2] = new EndOfLineRule("%", igComment);
		//rules[3] = new StringRule(igString);

		setPredicateRules(rules);
	}
}
