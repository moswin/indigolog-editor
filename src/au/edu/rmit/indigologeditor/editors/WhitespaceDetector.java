package au.edu.rmit.indigologeditor.editors;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

// Generic whitespace detector
public class WhitespaceDetector implements IWhitespaceDetector {

	public boolean isWhitespace(char c) {
		return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
	}
}
