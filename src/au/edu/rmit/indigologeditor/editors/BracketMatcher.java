package au.edu.rmit.indigologeditor.editors;

import java.util.List;
import java.util.Stack;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.wst.validation.AbstractValidator;
import org.eclipse.wst.validation.ValidationResult;
import org.eclipse.wst.validation.ValidationState;

// Used for detection of incorrect bracket layout.
public class BracketMatcher {
	public BracketMatcher() {
		
	}
	
	// Position looks like the best method of indicating what needs to be marked/highlighted.
	public static Position getUnmatchedBracket(IDocument doc) {
		// bracketStack used to check correct bracket closing order and all are matched
		Stack<BracketLocation> bracketStack = new Stack<BracketLocation>();
		String openBrackets = "{[(";
		String closeBrackets = "}])";
		
		String docText = doc.get();
		
		int charIndex = 0;
		int bracketIndex = 0;
		
		// Step through doc one char at a time
		while(charIndex < docText.length()) {
			char thisChar = docText.charAt(charIndex);
			// Check if char is an opening bracket, if so, add it to stack
			if(openBrackets.indexOf(thisChar) != -1) {
				bracketStack.add(new BracketLocation(new Position(charIndex, 1), thisChar));
			// Or see if it's a closing bracket, to be removed from the stack or detect a mismatch
			} else if((bracketIndex = closeBrackets.indexOf(thisChar)) != -1) {
				// If the stack is empty, we have a closing bracket without a matching open bracket
				if(bracketStack.size() == 0) {
					System.out.println("DEBUG -- Bracket mismatch");
					return new Position(charIndex, 1);
				}
				// Pop the opening bracket
				BracketLocation leftBracket = bracketStack.pop();
				
				// If it doesn't match, return the position of the closing bracket
				if(!leftBracket.isMatchingBracket(openBrackets.charAt(bracketIndex))) {
					return new Position(charIndex, 1);
				}
			}
			
			charIndex++;
		}
		
		// If the stack isn't empty after each char has been checked, we have unmatched brackets, return the inner-most bracket
		if(!bracketStack.empty())
			return bracketStack.pop().getPosition();
		
		return null;
	}
	
	// Stores a bracket type, and the position of it in the document
	public static class BracketLocation {
		private Position pos;
		private char bracket;
		
		public BracketLocation(Position pos, char bracket) {
			this.pos = pos;
			this.bracket = bracket;
		}
		
		boolean isMatchingBracket(char bracket) {
			return this.bracket == bracket;
		}
		
		Position getPosition() {
			return pos;
		}
	}
}
