package au.edu.rmit.indigologeditor.editors;

import java.awt.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public class ValidateFluents {
	private IResource resource;
	private Map fluents;
	private Map causes;
	
	public ValidateFluents(IResource res){
		this.resource = res;
		
		//find fluent declarations
		this.fluents = findFluents();
		
		//find fluents in causes
		this.causes = findCauses();
		
		//compare fluents and causes and mark any mismatches
		this.compare(fluents,causes);
		
	}
	
	public Map findFluents(){
		String fileName = IndiGologTextValidator.getFilePath(this.resource);
		String line;
		int lineIndex = 1;
		Map fluentsTemp = new HashMap();
		Pattern pattern = Pattern.compile("(?<=" + "fun_fluent" + "\\().*?(?=\\)\\.|\\)\\s*\\:|$)");
		
		// capture all the names of fluents in their declarations
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				Matcher m = pattern.matcher(line);
				if (m.find()) {
					System.out.println("Fluent: " + m.group() + " on line "
							+ lineIndex);
					fluentsTemp.put(m.group().toString().trim(), lineIndex);
				}
				lineIndex++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fluentsTemp;
	}
	
	public Map findCauses(){
		Map causesTemp = new HashMap();
		Map causesParameters = findAllCausesParameters();
				
		for(Object parameters : causesParameters.keySet()){
			StringBuilder buffer = new StringBuilder();
			
			int bracketsCounter = 0;
			int parameterNumber = 1;
			
			for(char c : parameters.toString().toCharArray()){
				if(c=='(') bracketsCounter++;
				if(c==')') bracketsCounter--;
				if(c==',' && bracketsCounter == 0){
					//add only the second parameter to tokens
					if(parameterNumber == 2){
						String key = causesParameters.get(parameters).toString();
						String value = buffer.toString();
						value = value.trim();
						causesTemp.put(key, value);
					}
					//clear the buffer
					buffer.delete(0, buffer.length());
					parameterNumber++;
				}
				else{
					buffer.append(c);
				}
			}
		}
		
		return causesTemp;
	}
	
	public Map findAllCausesParameters(){
		String fileName = IndiGologTextValidator.getFilePath(this.resource);
		String line;
		int lineIndex = 1;
		Map causesTemp = new HashMap();
		Pattern pattern = Pattern.compile("(?<=" + "causes" + "\\().*?(?=\\)\\.|\\)\\s*\\:|$)");
		
		//capture all the parameters of causes
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				Matcher m = pattern.matcher(line);
				if (m.find()) {
					System.out.println("Causes parameters: " + m.group() + " on line "
							+ lineIndex);
					causesTemp.put(m.group(), lineIndex);
				}
				lineIndex++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return causesTemp;
	}
	
	public void compare(Map f, Map c){
		System.out.println("comparing");
		for (Object fluent : c.keySet()) {
			if (!f.containsKey(c.get(fluent).toString())) {
				try {
					System.out.println("key: " + fluent + " value: "+ c.get(fluent));
					IndiGologMarkers.createMarker(this.resource,
							"No fluent " + c.get(fluent) + " has been defined on line " + fluent,
							Integer.parseInt(fluent.toString()));
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
