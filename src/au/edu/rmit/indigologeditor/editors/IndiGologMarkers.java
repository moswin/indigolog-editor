package au.edu.rmit.indigologeditor.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.MarkerUtilities;
import org.eclipse.ui.texteditor.SimpleMarkerAnnotation;

public class IndiGologMarkers {

	public static final String MARKER = "au.edu.rmit.indigologeditor.mymarkers.mymarker";
	public static final String ANNOTATION = "au.edu.rmit.indigologeditor.myannotation";

	public static IMarker createMarker(IResource res, String mes, int lineNum) throws CoreException {

		IMarker marker = res.createMarker(IMarker.PROBLEM);

		Map<String, Object> attributes = new HashMap<String,Object>();
		attributes.put(IMarker.LINE_NUMBER, lineNum);
		//attributes.put(IMarker.CHAR_START, 7);
		//attributes.put(IMarker.CHAR_END, 9);
		attributes.put(IMarker.SEVERITY, Integer.valueOf(IMarker.SEVERITY_ERROR));
		attributes.put(IMarker.MESSAGE, mes);
		attributes.put(IMarker.PRIORITY, Integer.valueOf(IMarker.PRIORITY_HIGH));
		marker.setAttributes(attributes);
		
		return marker;
	}
	
	public static IMarker createBracketMarker(IResource res, String mes, int lineNum) throws CoreException {
		IMarker marker = res.createMarker(IMarker.TASK);

		Map<String, Object> attributes = new HashMap<String,Object>();
		attributes.put(IMarker.LINE_NUMBER, lineNum);
		//attributes.put(IMarker.CHAR_START, 7);
		//attributes.put(IMarker.CHAR_END, 9);
		attributes.put(IMarker.SEVERITY, Integer.valueOf(IMarker.SEVERITY_ERROR));
		attributes.put(IMarker.MESSAGE, mes);
		attributes.put(IMarker.PRIORITY, Integer.valueOf(IMarker.PRIORITY_HIGH));
		marker.setAttributes(attributes);
		
		return marker;
	}

	public static List<IMarker> findMarkers(IResource resource) {
		try {
			return Arrays.asList(resource.findMarkers(MARKER, true,
					IResource.DEPTH_ZERO));
		} catch (CoreException e) {
			return new ArrayList<IMarker>();
		}
	}
	
	public static void deleteAllMarkers(IResource res){
		 int depth = IResource.DEPTH_INFINITE;
         try {
            res.deleteMarkers(IMarker.PROBLEM, true, depth);
         } catch (CoreException e) {
            // something went wrong
         }
	}
	
	public static void deleteBracketMarkers(IResource res) {
		int depth = IResource.DEPTH_INFINITE;
		try {
			res.deleteMarkers(IMarker.TASK, true, depth);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
