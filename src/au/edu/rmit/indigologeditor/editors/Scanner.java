package au.edu.rmit.indigologeditor.editors;

import org.eclipse.jface.text.rules.*;
import org.eclipse.jface.text.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;

// Provides a scanner to detect and set the color properties of predicates and keywords
public class Scanner extends RuleBasedScanner {

	public Scanner(ColorManager manager) {
		// Create various ITokens to assign text attributes
		IToken procInstr =
			new Token(
				new TextAttribute(
					manager.getColor(ColorConstants.PROC_INSTR)));
		
		IToken wordToken = 
			new Token(
				new TextAttribute(
					manager.getColor(ColorConstants.KEYWORD)));
		
		Token operatorToken = 
				new Token(
					new TextAttribute(
						manager.getColor(ColorConstants.OPERATOR)));
		
		IToken stringToken =
			new Token(
				new TextAttribute(
					manager.getColor(ColorConstants.IG_STRING)));
		
		IWordDetector wd = new IWordDetector() {
			public boolean isWordPart(char character) {
				return Character.isJavaIdentifierPart(character);
			}
			
			public boolean isWordStart(char character) {
				return Character.isJavaIdentifierStart(character);
			}
		};
		
		// define predicates
		WordRule wordRule = new WordRule(wd);
		wordRule.addWord("prim_action", wordToken);
		wordRule.addWord("prim_fluent", wordToken);
		wordRule.addWord("causes_val", wordToken);
		wordRule.addWord("causes", wordToken);
		wordRule.addWord("poss", wordToken);
		wordRule.addWord("initially", wordToken);
		wordRule.addWord("rejects", wordToken);
		wordRule.addWord("settles", wordToken);
		wordRule.addWord("proc", wordToken);
		wordRule.addWord("senses", wordToken);
		wordRule.addWord("exog_action", wordToken);
		wordRule.addWord("cache", wordToken);
		wordRule.addWord("fun_fluent", wordToken);
		
		// define keywords
		OperatorRule operatorRule = new OperatorRule(wd);
		operatorRule.addWord("and", operatorToken);
		operatorRule.addWord("or", operatorToken);
		operatorRule.addWord("implies", operatorToken);
		operatorRule.addWord("neg", operatorToken);
		operatorRule.addWord("true", operatorToken);
		operatorRule.addWord("false", operatorToken);
		
		IRule[] rules = new IRule[4];
		
		rules[0] = new MultiLineRule("{", "}", procInstr);
		// Add generic whitespace rule.
		rules[1] = new WhitespaceRule(new WhitespaceDetector());
		rules[2] = wordRule;
		rules[3] = operatorRule;
		
		setRules(rules);
	}
}
